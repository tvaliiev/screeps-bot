export class MemoryCache {
  private readonly CACHE_VERSION_PREFIX = '__version'
  private readonly version: string
  private readonly prefix: string

  constructor(prefix: string = '', version: string = '1') {
    this.version = version

    if (prefix === this.CACHE_VERSION_PREFIX) {
      throw new Error('Это запрещенное значение для префикса кэша')
    }

    this.prefix = prefix

    this.initMemoryCell()
  }

  private initMemoryCell() {
    if (!Memory.cache) {
      Memory.cache = {}
    }

    if (!Memory.cache[this.CACHE_VERSION_PREFIX]) {
      Memory.cache[this.prefix] = {}
      Memory.cache[this.CACHE_VERSION_PREFIX] = this.version
    } else if (Memory.cache[this.CACHE_VERSION_PREFIX] !== this.version) {
      this.dropAllCache()
    }
  }

  public dropAllCache() {
    delete Memory.cache[this.CACHE_VERSION_PREFIX]
    this.initMemoryCell()
  }

  public get<T>(key: string): T | undefined {
    // @ts-ignore
    return Memory.cache[this.prefix][key]
  }

  public set<T>(key: string, value: T) {
    // @ts-ignore
    Memory.cache[this.prefix][key] = value
  }
}



export class GarbageCollector {
  private static garbageCollectionLimit = Math.floor(Game.cpu.tickLimit * 1.5);

  public static collectGarbage() {
    if (Game.cpu.bucket < this.garbageCollectionLimit) {
      return;
    }

    // Automatically delete memory of missing creeps
    for (const name in Memory.creeps) {
      if (!(name in Game.creeps)) {
        delete Memory.creeps[name];
      }
    }
  }
}

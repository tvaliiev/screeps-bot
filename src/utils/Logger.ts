import {LFService, LoggerFactory, LoggerFactoryOptions, LogGroupRule, LogLevel} from "typescript-logging";

const options = new LoggerFactoryOptions();
options.addLogGroupRule(new LogGroupRule(new RegExp("creeps.+"), LogLevel.Info));
options.addLogGroupRule(new LogGroupRule(new RegExp("BotEngine"), LogLevel.Info));
options.addLogGroupRule(new LogGroupRule(new RegExp("main"), LogLevel.Info));

export const factory: LoggerFactory = LFService.createNamedLoggerFactory("app", options);

const Constants = {
  CREEP_ROLE: {
    BUILDER: 'builder',
    HARVESTER: 'harvester',
    UPGRADER: 'upgrader',
    WARRIOR: 'warrior',
    NONE: '!none!',
  },
  MAIN_SPAWN_NAME: 'Main Spawn',
  CACHE: {
    CREEP_BUILDER: {
      PARTS_COSTS_BY_ROLE_CACHE: 'partsCostsByRoleCache',
      CREEP_COUNT_BY_ROLE: 'creepCountByRole',
      BUILDING_PRIORITY: 'buildingPriority',
    }
  }
}

export {Constants}

import {ErrorMapper} from "utils/ErrorMapper"
import {BotEngine} from "./BotEngine"
import {Constants} from "./constants";
import {factory} from "./utils/Logger";

if (!Game.spawns[Constants.MAIN_SPAWN_NAME]) {
  throw new Error('Отсутствует главный спавн')
}

const logger = factory.getLogger('main')
logger.info(`Загрузка движка...`)

const engine = new BotEngine(Game.spawns[Constants.MAIN_SPAWN_NAME])


export const loop = ErrorMapper.wrapLoop(() => {
  engine.loop()
})

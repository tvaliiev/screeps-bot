type CreepRole = string

interface CreepMemory {
  role: CreepRole
  working: boolean
}

interface CreepBuilderConfig {
  [name: string]: CreepBuilderConfigItem
}

interface CreepBuilderConfigItem {
  count: number
  parts: BodyPartConstant[]
  role: CreepRole
  weight: number
}

interface Memory {
  uuid: number
  log: any
  minimumBucketSize: number
  cache: { [prefix: string]: { [key: string]: CacheCell } | string }
}

type CacheCell = CreepCountByRoleCountCache | PartsCostsByRoleCache | BuildingPriorityCache

type BuildingPriorityCache = Array<{ role: CreepRole, totalWeight: number }>

interface PartsCostsByRoleCache {
  [role: string]: number[]
}

interface CreepCountByRoleCountCache {
  [role: string]: number
}

// `global` extension samples
declare namespace NodeJS {
  interface Global {
    log: any
  }
}

import {Constants} from "../constants"
import {factory} from "../utils/Logger";
import {MemoryCache} from "../utils/MemoryCache";
import {CreepFactory} from "./CreepFactory";

const logger = factory.getLogger('creeps.CreepBuilder')

export class CreepBuilder {
  private readonly cacheVersion = '2';

  private creepBuildConfig: CreepBuilderConfig = {
    [Constants.CREEP_ROLE.HARVESTER]: {
      role: Constants.CREEP_ROLE.HARVESTER,
      count: 3,
      parts: [MOVE, WORK, CARRY, MOVE, WORK, WORK, CARRY],
      weight: 100,
    },
    [Constants.CREEP_ROLE.BUILDER]: {
      role: Constants.CREEP_ROLE.BUILDER,
      count: 2,
      parts: [MOVE, WORK, CARRY, MOVE, WORK, WORK, CARRY],
      weight: 50,
    },
    [Constants.CREEP_ROLE.UPGRADER]: {
      role: Constants.CREEP_ROLE.UPGRADER,
      count: 1,
      parts: [MOVE, WORK, CARRY, MOVE, WORK, WORK, CARRY],
      weight: 100
    }
  }

  private cache: MemoryCache = new MemoryCache('creeps__CreepBuilder', this.cacheVersion)

  private spawn: StructureSpawn

  constructor(spawn: StructureSpawn) {
    this.spawn = spawn
  }

  private getCreepCountByRole() {
    const valueFromCache = this.cache.get<CreepCountByRoleCountCache>(Constants.CACHE.CREEP_BUILDER.CREEP_COUNT_BY_ROLE)

    if (valueFromCache) {
      return valueFromCache
    }

    logger.info('getCreepCountByRole - Отсутствуют данные в кэше')

    return this.recalculateCreepCountByRole()
  }

  private recalculateCreepCountByRole(): CreepCountByRoleCountCache {
    const creepCountByRole: CreepCountByRoleCountCache = {}
    Object.values(Constants.CREEP_ROLE).forEach((role) => {
      creepCountByRole[role] = 0
    })

    for (const creepName in Game.creeps) {
      const role = Game.creeps[creepName].memory.role;
      if (creepCountByRole.hasOwnProperty(role)) {
        creepCountByRole[role]++
      }
    }

    this.cache.set<CreepCountByRoleCountCache>(Constants.CACHE.CREEP_BUILDER.CREEP_COUNT_BY_ROLE, creepCountByRole)

    return creepCountByRole
  }

  private getBuildingPriority(): BuildingPriorityCache {
    const fromCache = this.cache.get<BuildingPriorityCache>(Constants.CACHE.CREEP_BUILDER.BUILDING_PRIORITY)

    if (fromCache) {
      return fromCache
    }

    logger.info('getBuildingPriority - Отсутствуют данные в кэше')

    return this.recalculateBuildingPriority()
  }

  private recalculateBuildingPriority(): BuildingPriorityCache {
    const buildingPriority = []
    for (const role in this.creepBuildConfig) {
      const el = this.creepBuildConfig[role]

      buildingPriority.push({
        totalWeight: el.weight * (el.count - this.getCreepCountByRole()[role]),
        role,
      })
    }

    const creepCountByRole: CreepCountByRoleCountCache = this.getCreepCountByRole()
    buildingPriority.sort((a, b) => {
      return b.totalWeight === a.totalWeight ?
        creepCountByRole[b.role] - creepCountByRole[a.role] :
        b.totalWeight - a.totalWeight
    })

    this.cache.set<BuildingPriorityCache>(Constants.CACHE.CREEP_BUILDER.BUILDING_PRIORITY, buildingPriority)

    return buildingPriority
  }

  private getPartsCostsByRole(): PartsCostsByRoleCache {
    const valueFromCache = this.cache.get<PartsCostsByRoleCache>(Constants.CACHE.CREEP_BUILDER.PARTS_COSTS_BY_ROLE_CACHE)

    if (valueFromCache) {
      return valueFromCache
    }

    logger.info('getPartsCostsByRole - Отсутствуют данные в кэше')

    return this.recalculateCreepRoleCosts();
  }

  private recalculateCreepRoleCosts() {
    const partsCostsByRole: PartsCostsByRoleCache = {}
    for (const creepRole in this.creepBuildConfig) {
      let costCache: number = 0
      partsCostsByRole[creepRole] = this.creepBuildConfig[creepRole].parts.map((partName: BodyPartConstant) => {
        return costCache += BODYPART_COST[partName]
      })
    }

    this.cache.set<PartsCostsByRoleCache>(Constants.CACHE.CREEP_BUILDER.PARTS_COSTS_BY_ROLE_CACHE, partsCostsByRole)

    return partsCostsByRole
  }

  public buildMissingCreeps() {
    if (this.spawn.spawning) {
      logger.info(`buildMissingCreeps - Спавнер всё еще спавнит юнита, пропускаю ход`)
      return
    }

    this.buildCreepByRole(this.getBuildingPriority()[0].role)
  }

  private buildCreepByRole(roleName: CreepRole): ScreepsReturnCode | null {
    if (!this.creepBuildConfig[roleName]) {
      logger.warn(`buildMissingCreeps - Отсутствует задача по строительству юнитов для роли ${roleName}`)
      return null
    }

    const buildingMapItem: CreepBuilderConfigItem = this.creepBuildConfig[roleName]

    if (this.getCreepCountByRole()[buildingMapItem.role] >= buildingMapItem.count) {
      logger.info(`buildMissingCreeps - уже построено необходимое количество юнитов для роли`)
      return null
    }

    return this.buildMissingCreep(buildingMapItem)
  }

  private buildMissingCreep(mapItem: CreepBuilderConfigItem): ScreepsReturnCode | null {
    const creepName = `${mapItem.role}_${this.getCreepCountByRole()[mapItem.role] + 1}`;

    const maxSpawnEnergy = this.spawn.store.getCapacity(RESOURCE_ENERGY)
    const costIndex = _.findLastIndex(this.getPartsCostsByRole()[mapItem.role], cost => cost <= maxSpawnEnergy)
    const body = mapItem.parts.slice(0, costIndex + 1)

    logger.info(`buildMissingCreep - Пробую создать крипа для роли ${mapItem.role} c именем ${creepName}`)
    const response = this.spawn.spawnCreep(body, creepName, {
      memory: CreepFactory.getDefaultMemoryByRole(mapItem.role)
    });

    if (response === OK) {
      this.recalculateCreepCountByRole()
      this.recalculateBuildingPriority()
      return response
    }

    if (response === ERR_NOT_ENOUGH_ENERGY) {
      const allPartsCost = this.getPartsCostsByRole()[mapItem.role][costIndex];
      const spawnUsedCapacity = this.spawn.store.getUsedCapacity(RESOURCE_ENERGY);
      logger.debug(`buildMissingCreep - не хватает материалов для строительства роли ${mapItem.role} (${spawnUsedCapacity}/${allPartsCost})`)
      return response
    }

    logger.warn(`buildMissingCreep - Ошибка при сторительстве юнита ${response}`)
    return response
  }
}

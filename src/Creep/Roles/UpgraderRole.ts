import {Constants} from "../../constants";
import {BaseCreep} from "../BaseCreep";

export class UpgraderRole extends BaseCreep {
  public static getDefaultMemory(): CreepMemory {
    return Object.assign(super.defaultMemory, {
      role: Constants.CREEP_ROLE.UPGRADER,
    })
  }

  public run(): void {
    if (this.creep.memory.working && this.creep.store[RESOURCE_ENERGY] === 0) {
      this.creep.memory.working = false;
      this.creep.say('🔄 harvest');
    }

    if (!this.creep.memory.working && this.creep.store[RESOURCE_ENERGY] === this.creep.store.getCapacity(RESOURCE_ENERGY)) {
      this.creep.memory.working = true;
      this.creep.say('⚡ upgrade');
    }

    if (this.creep.memory.working) {
      // @ts-ignore
      if (this.creep.upgradeController(this.creep.room.controller) === ERR_NOT_IN_RANGE) {
        // @ts-ignore
        this.creep.moveTo(this.creep.room.controller, {visualizePathStyle: {stroke: '#00ff00'}});
      }
    } else {
      const sources = this.creep.room.find(FIND_SOURCES);
      if (this.creep.harvest(sources[0]) === ERR_NOT_IN_RANGE) {
        this.creep.moveTo(sources[0], {visualizePathStyle: {stroke: '#00ff7b'}});
      }
    }
  }
}

import {Constants} from "../../constants";
import {BaseCreep} from "../BaseCreep";

export class HarvesterRole extends BaseCreep {
  public static getDefaultMemory(): CreepMemory {
    return Object.assign(super.defaultMemory, {
      role: Constants.CREEP_ROLE.HARVESTER,
    })
  }

  public run() {
    if (this.creep.store[RESOURCE_ENERGY] < this.creep.store.getCapacity()) {
      const sources = this.creep.room.find(FIND_SOURCES);
      if (this.creep.harvest(sources[0]) === ERR_NOT_IN_RANGE) {
        this.creep.moveTo(sources[0], {visualizePathStyle: {stroke: '#007bff'}});
      }
    } else {
      const targets = this.creep.room.find(FIND_STRUCTURES, {
        filter: (structure) => {
          return (structure.structureType === STRUCTURE_EXTENSION ||
            structure.structureType === STRUCTURE_SPAWN ||
            structure.structureType === STRUCTURE_TOWER) && structure.energy < structure.energyCapacity;
        }
      });
      if (targets.length > 0) {
        if (this.creep.transfer(targets[0], RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
          this.creep.moveTo(targets[0], {visualizePathStyle: {stroke: '#0000ff'}});
        }
      }
    }
  }
}

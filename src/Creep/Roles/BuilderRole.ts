import {Constants} from "../../constants";
import {BaseCreep} from "../BaseCreep";

export class BuilderRole extends BaseCreep {
  public static getDefaultMemory(): CreepMemory {
    return Object.assign(super.defaultMemory,{
      role: Constants.CREEP_ROLE.BUILDER,
    })
  }

  public run() {
    if (this.creep.memory.working && this.creep.store.energy === 0) {
      this.creep.memory.working = false;
      this.creep.say('🔄 harvest');
    }

    if (!this.creep.memory.working && this.creep.store.energy === this.creep.store.getCapacity()) {
      this.creep.memory.working = true;
      this.creep.say('🚧 build');
    }

    if (this.creep.memory.working) {
      const targets = this.creep.room.find(FIND_CONSTRUCTION_SITES);
      if (targets.length) {
        if (this.creep.build(targets[0]) === ERR_NOT_IN_RANGE) {
          this.creep.moveTo(targets[0], {visualizePathStyle: {stroke: '#feff00'}});
        }
      }
    } else {
      const sources = this.creep.room.find(FIND_SOURCES);
      if (this.creep.harvest(sources[0]) === ERR_NOT_IN_RANGE) {
        this.creep.moveTo(sources[0], {visualizePathStyle: {stroke: '#ffaa00'}});
      }
    }
  }
}

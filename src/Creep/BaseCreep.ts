import {Constants} from "../constants";

export abstract class BaseCreep {
  protected static defaultMemory = {
    role: Constants.CREEP_ROLE.NONE,
    working : false,
  }
  protected creep: Creep

  constructor(creep: Creep) {
    this.creep = creep
  }

  public abstract run(): void

  public static getDefaultMemory(): CreepMemory {
    return this.defaultMemory;
  }

  public getRole(): string {
    return this.creep.memory.role
  }
}


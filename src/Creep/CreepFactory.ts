import {Constants} from "../constants";
import {BaseCreep} from "./BaseCreep";
import {BuilderRole} from "./Roles/BuilderRole";
import {HarvesterRole} from "./Roles/HarvesterRole";
import {UpgraderRole} from "./Roles/UpgraderRole";


export class CreepFactory {
  public static wrapDefaultCreep(creep: Creep): BaseCreep {
    switch (creep.memory.role) {
      case Constants.CREEP_ROLE.BUILDER:
        return new BuilderRole(creep)
      case Constants.CREEP_ROLE.HARVESTER:
        return new HarvesterRole(creep)
      case Constants.CREEP_ROLE.UPGRADER:
        return new UpgraderRole(creep)
      default:
        throw new Error('No such creep role: ' + creep.memory.role)
    }
  }

  public static getDefaultMemoryByRole(role: CreepRole): CreepMemory {
    switch (role) {
      case Constants.CREEP_ROLE.BUILDER:
        return BuilderRole.getDefaultMemory()
      case Constants.CREEP_ROLE.HARVESTER:
        return HarvesterRole.getDefaultMemory()
      case Constants.CREEP_ROLE.UPGRADER:
        return UpgraderRole.getDefaultMemory()
      default:
        throw new Error('No such creep role: ' + role)
    }
  }
}

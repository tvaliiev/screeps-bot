import {CreepBuilder} from "./Creep/CreepBuilder";
import {CreepFactory} from "./Creep/CreepFactory";
import {GarbageCollector} from "./utils/GarbageCollector";
import {factory} from "./utils/Logger";

const logger = factory.getLogger('BotEngine')

export class BotEngine {
  private readonly minimumBucketSize: number = Math.floor(Game.cpu.tickLimit * 2)
  private readonly spawn: StructureSpawn
  private readonly creepBuilder: CreepBuilder

  constructor(spawn: StructureSpawn) {
    this.spawn = spawn
    this.creepBuilder = new CreepBuilder(this.spawn)
  }

  public loop() {
    logger.info(`Текущий тик ${Game.time}`)
    if (this.shouldSkipLoop()) {
      logger.info(`Пропускаю этот тик...`)
      return;
    }

    this.processCreeps()

    // this.renewCreeps()
    this.buildCreeps()

    GarbageCollector.collectGarbage()
  }


  private processCreeps() {
    for (const name in Game.creeps) {
      const creep = CreepFactory.wrapDefaultCreep(Game.creeps[name]);
      creep.run();
    }
  }

  private renewCreeps() {

  }

  public buildCreeps() {
    this.creepBuilder.buildMissingCreeps()
  }

  public shouldSkipLoop() {
    return !('sim' in Game.rooms) && Game.cpu.bucket < this.minimumBucketSize
  }
}


